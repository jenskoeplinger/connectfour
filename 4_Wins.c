/*                                4 Gewinnt

Spielfeld: 5                  anfangender Spieler: 1
           4                  anderer:             2
           3
           2
           1
           0
             0 1 2 3 4 5 6

Die Stellungsbewertung prueft, wie gut die vorgegebenen Gewinnmuster
auf die jeweilige Stellung passen. Datenformat eines Musters:

Anzahl der relevanten Stellen, max x, max y zum Testen (immer linke
untere Ecke); danach die Relativkoordinaten und der Typ des Feldes

Typ 0: Leerstelle mit Stein/Rand darunter
Typ 1: eigener Spielstein

Dankesbekundungen an:

Jens K�plinger
jkoeplin@ix.urz.uni-heidelberg.de

*/

#include<stdio.h>
#include<stdlib.h>
#define anzahl 7
#define steinmax 5 /* 7 Gewinnmuster, maximal 5 Steine */

int steine[anzahl]={4,4,4,4,5,5,5}; /* Anzahl der relevanten Steine */
int xmax[anzahl]={3,3,6,3,2,2,2}; /* max.X-Position bei der das Muster zu
                                     testen ist */
int ymax[anzahl]={5,2,2,2,5,1,1}; /* maximale Y-Position */

/* Die relativangaben xrel und yrel sind immer von links unten gesehen */
int xrel[anzahl][steinmax] = {0,1,2,3,-1, 0,1,2,3,-1, 0,0,0,0,-1, 
                             0,1,2,3,-1, 0,1,2,3,4, 0,1,2,3,4, 0,1,2,3,4};
int yrel[anzahl][steinmax] = {0,0,0,0,-1, 0,1,2,3,-1, 0,1,2,3,-1, 3,2,1,0,-1, 
                             0,0,0,0,0, 4,3,2,1,0, 0,1,2,3,4};
/* Typ -1 oder xrel/yrel -1 wird nie aufgerufen. Ist negativ gesetzt, um im
   Falle eines Programmfehlers eine Fehlermeldung zu provozieren */
int typ[anzahl][steinmax]  = {1,1,1,1,-1, 1,1,1,1,-1, 1,1,1,1,-1, 1,1,1,1,-1, 
                             0,1,1,1,0, 0,1,1,1,0, 0,1,1,1,0};

long werte1[7],werte2[7]; /* fuer Zwischenwertspeicherung bei
                                 Compterlogik */
int hoechst=0; /* hoechste besetzte Reihe */
int feld[7][7]; /* eigentlich ja nur 6 Reihen hoch
                  Struktur: links/unten Spalte=0/Reihe=0 */
int maxtiefe; /* maximale Rechentiefe in Halbzuegen */
long dummy;
int setspalte; /* Spalte, in die der Computer effektiv setzen will */
int erlaub[8]; /* um bestimmte Zuege zu verbieten, weil der Gegner gewinnen
                  wuerde */

/* --------------------------------------------------------------------- */
/*                                 main                                  */
/* --------------------------------------------------------------------- */

void main()
{
  int g,h,flag;
  int mode,spalte;
  int zug;
  void comp(int);
  void spiel(int);
  void print();
  void comp(int);

  g=7; while(g--) { h=7; while(h--) feld[g][h]=0; }

  maxtiefe=2;

  do
  {
    printf("\nTippen Sie ...\n");
    printf("0 fuer Computer-Computer\n");
    printf("1 fuer Spieler -Computer\n");
    printf("2 fuer Computer-Spieler\n");
    printf("3 fuer Spieler -Spieler\n");
    printf("4 fuer Rechentiefe einstellen\n");
    printf("Rechentiefe ist %d\n",maxtiefe);
    scanf("%d",&mode);
    if (mode==4)
      {
      printf("Geben Sie die Rechentiefe ein (0 bis ...)\n");
      do
        scanf("%d",&maxtiefe);
      while ((maxtiefe<0)||(maxtiefe>1000));
      }
  }
  while ((mode>3)||(mode<0));
  printf("\nOk.\n");

  zug=0; /* 0 = Spieler 1 zieht, 1 = Spieler 2 zieht */

  while (1)
  {
    printf("Spieler %1d\n",zug+1);
    switch(mode)
    {
      case 0: { comp(zug); break; }
      case 1: { if (zug) comp(zug); else spiel(zug); break; }
      case 2: { if (zug) spiel(zug); else comp(zug); break; }
      case 3: { spiel(zug); break; }
    }
    print();
    zug=1-zug;
  }
}
/* ------------------------------- spiel ------------------------------- */
/* Spieler gibt seinen Zug ein */
void spiel(int zug)
{
  int spalte, flag;
  int add(int,int);
  do
  {
    do
    {
      printf("Geben Sie bitte die Spalte an (1..7):\n");
      scanf("%d",&spalte);
    }
    while((spalte<1)||(spalte>7));
    flag=add(spalte-1,zug+1);
  }
  while (flag); /* wenn die Reihe schon voll ist, nochmal eingeben */
  return;
}

/* -------------------------------- add --------------------------------- */
/* setzt einen "spieler"-Stein in "spalte" */
int add(int spalte,int spieler)
{
  int flag, g;
  flag=0;
  g=0;
  while (feld[spalte][g]) g++;
  if (g<6)
    {
      feld[spalte][g]=spieler;
      if (g>hoechst) hoechst=g;
    }
    else flag=1; /* wenn Reihe schon voll */
  return flag;
}

/* --------------------------------- sub ------------------------------- */
/* loescht den obersten Stein einer Reihe */
void sub(int spalte)
{
  int g;
  g=0;
  while (feld[spalte][g]) g++;

  if (g) feld[spalte][g-1]=0;
  return;
}

/* ------------------------------ calculate ----------------------------- */
/* calculate macht Stellungsbewertung
   In: Spieler, dessen Stellung berechnet werden soll
       1 - Spieler, der angefangen hat
       2 - anderer
   ziehflag = 0 wenn Spieler gerade gezogen hat
            = 1 wenn Spieler noch ziehen darf
     (wirkt sich aus, wenn es sich bei einer Luecke in einem der Modelle
      um eine handelt, die der Spieler genau im naechsten Zug besetzen
      kann => wert wird mit 10 multipliziert)
   OUT wert
   calculate geht alle gespeicherten Muster nacheinander durch und schaut,
   wie gut sie auf das Spielfeld passen; je nachdem wieviele Luecken da
   sind und um welche Luecken es sich handelt, werden Punkte vergeben.
   Luecken, die sich in der Schluss-Zugzwangphase positiv auswirken werden,
   sind natuerlich viel wert
*/

long calculate(int spieler, int ziehflag)
{
  long wert;
  int g,h,t,st,modell,zahl;
  int xpos,ypos,xr,yr;
  int falsch; /* Anzahl der Luecken oder <0 falls unmoeglich */
  int leer;   /* =0, =1 wenn es sich um eine unmittelbar besetzbare
                 Luecke handelt (nur wenn ziehflag=1) */
  int zwang;  /* Indikator auf Fallen durch Zugzwang am Ende */
  int zwanghoehe; /* hoehe, in der es zu einer Zwangssituation kam */

  modell=anzahl;
  wert=0;

  while (modell--) /* zaehlt die Modelle durch */
  {
    xpos=xmax[modell]+1; /* zaehlt alle x-Positionen durch */
    while (xpos--)
    {
      ypos=ymax[modell]+1;
      while(ypos--) /* zaehlt die y-Positionen durch */
      {
        falsch=0;
        leer=0;
        zahl=steine[modell];
        if (ypos>hoechst) { zahl=0; falsch=3; }
        zwang=0;
        while (zahl--) /* ueberpfueft jeden Stein des Musters einzeln */
        {
          t=typ[modell][zahl];
          xr=xpos+xrel[modell][zahl];
          yr=ypos+yrel[modell][zahl];
          switch(t)
          {
          case 0: /* Leerstelle mit Stein darunter */
            {
              st=feld[xr][yr];
              if (st) falsch=-100;
                else
                  if (yr)
                    if (feld[xr][yr-1]==0)
                    {
                      falsch++;
                      if (ziehflag)
                      {
                        if (yr==1) leer=1;
                          else
                          if (feld[xr][yr-2]) leer=1;
                      }
                    }
              break;
            }
          case 1: /* eigener Stein */
           {
              st=feld[xr][yr];
              if (st==(3-spieler)) falsch=-100;
                else
                  if (!st)
                    {
                      falsch++;
                      /* --------------- */
                      /*     ZUGZWANG    */
                      /* --------------- */
                      if (spieler==1)
                      {
                        if ((yr==2)||(yr==4))
                        {
                          zwang++;
                          zwanghoehe=yr;
                        }
                      }
                      else
                      {
                        if ((yr==1)||(yr==3)||(yr==5))
                        {
                          zwang++;
                          zwanghoehe=yr;
                        }
                      }
                      
                      /* -- Ende Zugzwangtest ++ */

                      if (ziehflag)
                        {
                        if (!yr) leer=1;
                          else
                            if (feld[xr][yr-1]) leer=1;
                        }
                    }
              break;
            }
          } /* End of Switch */
        
          if (falsch<0) zahl=0;
          if (falsch>2) zahl=0;
        
        } /* End of while zahl */

        if ((falsch>=0)&&(falsch<3))
        {
          if (!falsch)
            {
            if (modell<4) wert+=1e5; /* Spieler kann unmittelbar gewinnen */
              else wert+=1e4; /* Spieler gewinnt zwingend in 2 Zuegen */
            }
          if ((falsch==1)&&(leer==1))
            {
            if (modell<4) wert+=1e5; /* Gegenspieler kann gewinnen */
              else wert+=1e4; /*Gegenspieler gewinnt zwingend in 2 Zuegen */
            }
          if ((leer)||(zwang)) falsch--; /*Bevorteilung von Zw.szugstellen*/
          if ((falsch==1)&&(zwang))
            wert+=(6-zwanghoehe)*200; /* tiefe Zwangszugst. sind wertvoller*/
          switch(falsch)
          {
          case 0: { wert+=1000; break; } /* alle Steine sind richtig */
          case 1: { wert+=50; break; } /* ein Stein ist falsch */
          case 2: { wert+=1; break; } /* zwei Steine sind falsch */
          } /* End of switch */
        } /* End of if */
      }  /* End of while ypos */
    } /* End of while xpos */
  } /* End of while modell */
  return wert;

} /* End of function calculate */


/* -------------------------------- print ------------------------------- */
/* druckt das Spielfeld aus */
void print()
{
  int zeile=6;
  int spalte;

  printf("\n\n");
  while (zeile--)
    {
      spalte=0;
      do
      {
        switch (feld[spalte][zeile])
        {
          case 0: { printf(" ."); break; }
          case 1: { printf(" x"); break; }
          case 2: { printf(" o"); break; }
        }
      }
      while (++spalte<7);
      printf("\n");
    }
  printf("-1-2-3-4-5-6-7-\n\n");
  return;
}


/* ------------------------------------------------------------------- */
/*                                                                     */
/*                   COMPUTERLOGIK - HERZSTUECK                        */
/*                                                                     */
/* Hier wird die Prozedur calculate rekursiv aufgerufen                */
/* Einsprung: tiefe=maxtiefe                                           */

long rekur(int zug,int tiefe)
{
  long diff1[7];
  void sub(int);
  int add(int,int);
  long diff,maxdiff;
  int spalte,flag;
  long rekur(int,int);
  long calculate(int,int);

  spalte=7; while(spalte--) diff1[spalte]=-1e6; /* Alte Eintraege
                                                   zuruecksetzen */
  spalte=7;
  while (spalte--)
  {
    if (tiefe==maxtiefe) /* also nur im allerersten Zug */
      while(!erlaub[spalte]) spalte--; /*alle unerlaubten Spalten
                                         herunterzaehlen */
    if (spalte<0) break; /* unschoen, ich weiss, aber uebersichtlicher */

    flag=add(spalte,zug+1);
    if (flag)
      {
        diff1[spalte]=-2e6;
      }
      else
      {
        if (tiefe)
          {
            diff1[spalte]=-rekur(1-zug,tiefe-1);
          }
          else
          {
            werte1[spalte]=calculate(zug+1,0);
            werte2[spalte]=calculate(2-zug,1);
            if (werte1[spalte]>1e5) werte2[spalte]=-1e5;
              else if (werte1[spalte]>1e4)
                     if (werte2[spalte]<=1e5) werte2[spalte]=-1e5;
            diff1[spalte]=werte1[spalte]-werte2[spalte];
          }
        sub(spalte);
      }
  }
  maxdiff=-1e6;
  spalte=7;
  while(spalte--)
  {
    if (diff1[spalte]>maxdiff)
    {
      maxdiff=diff1[spalte];
      setspalte=spalte;
    }
  }
  if (tiefe==maxtiefe) /* also kurz vor dem Endgueltigen Verlassen */
  {
    spalte=7;
    while(spalte--)
    {  
/*    printf("Tiefe %d Spalte %d Bewertung %ld\n",maxtiefe,spalte+1,
              diff1[spalte]);
 */      if (diff1[spalte]<-1e4) /*verbietet alle Spalten, in denen der */
        erlaub[spalte]=0;     /* Gegner gewinnen wuerde */
    }
/*  printf(". Tiefe %d Verbotene Spalten: ",tiefe);
    spalte=7;    while(spalte--)      if (!erlaub[spalte]) printf("%d ",spalte+1);    printf("\n"); */  }
   return maxdiff;
}

/* ------------------------------------ comp ------------------------- */
/* comp ruft rekur zunaechst mit 0 Halbzuegen Rechentiefe, dann mit
   einem usw. bis maxtiefe auf. Dabei streicht rekur nacheinander die
   Spalten aus, in welchen der Gegner vorzeitig gewinnen wuerde.  */

void comp(int zug)
{
  long rekur(int,int);
  int tiefe=0, g, zaehler, flag;
  int merktiefe;
  long maxdiff, feldwert;
  int add(int,int);
  long calculate(int,int);
  
  g=7; while(g--) erlaub[g]=1; /*zunaechst alle Spalten erlaubt*/
  merktiefe=maxtiefe;
  do
  {
    maxtiefe=tiefe;
    maxdiff=rekur(zug,tiefe);
    
    g=7;
    while(g--) zaehler+=erlaub[g];
    if (zaehler==0) /* wenn alle 7 Spalten zum zwingenden Verlust fuehren */
      {
        printf("Schoenes Wetter heute, nicht wahr?\n");
        g=7;
        while(g--) erlaub[g]=1; /* ... dann soll er wieder so tun, als
                                   waere gar nichts */
      }
    if ((zaehler==1)||(zaehler==2))
      printf("Da hat er mir ja eine Falle gestellt!\n");

    if (maxdiff>1e5) tiefe=merktiefe; /* wenn Gewinnzug, sofort ausfuehren */
       /* ein >1e4 koennte hier den Prozess etwas beschleunigen, ich bin mir
          nur nicht ganz sicher, ob er dann auch wirklich schon rechtzeitig
          abbricht; evtl. waere ein >2e4 sicher */
  }
  while(++tiefe<=merktiefe);
  maxtiefe=merktiefe;

/*  printf("Stellungsbewertung fuer mich: %ld\n",maxdiff);
 */  printf("Ich setze Spalte %1d\n",setspalte+1);
  if (maxdiff>0) printf("Ein Meisterzug.\n");
  if (maxdiff<0) printf("Bald stehe ich wieder besser, wart's nur ab!\n");
  flag=add(setspalte,zug+1);
  if (flag) { printf("???????? fataler Fehler"); exit(0); }
  
  feldwert=calculate(zug+1,0);
  if (feldwert>1e5)
    {
       printf("\n");
       print();
       printf("\nIch habe gewonnen. Was auch sonst.\n");
       exit(0);
    }
  if (maxdiff>1e5)
    printf("Ich glaube, ich werden gewinnen!\n");
  else
    if (maxdiff>1e4)
      printf("Pass auf, ich glaube, Du hast keine Chanze mehr!\n");

  if (maxdiff<-1e5)
    printf("Der letzte Zug von Dir war doch hoffentlich nur ein Scherz!\n");
  else
    if (maxdiff<-1e4)
      printf("Du siehst es ja doch nicht, wie Du jetzt gewinnen kannst!\n");

  feldwert=calculate(2-zug,0);
  if (feldwert>1e5)
    {
       printf("Da kann ich nichts dafuer! Schuld ist allein mein\n");
       printf("Programmierer! Jens, wo bist Du? Komm sofort her!\n");
       exit(0); /* Dieser Ausdruck sollte ab Rechentiefe 2 nie mehr
                   aufgerufen werden, hoffe ich ! */
    }
}






